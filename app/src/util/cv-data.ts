import { CVData, protectedURL } from './commonTypes';
import { client } from './axiosClient';

export let cvData: CVData;

export async function getCVData() {
    const cvDataURL = `${protectedURL}cvData.json`;
    console.log(`Getting CV Data: ${cvDataURL}`);
    const response = await client.get(cvDataURL);
    cvData = response.data;
}