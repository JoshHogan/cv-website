import { client } from "./axiosClient";
import { apiServerURL } from "./commonTypes";

const pingTimeout = 10 * 1000;

export function setupPing()
{
    console.log("Ping Setup: " + pingTimeout)
    setTimeout(ping, pingTimeout);
}

async function ping()
{
    try {
        console.log("Ping");
        await client.get(apiServerURL + "ping");
        setTimeout(ping, pingTimeout);
    } catch (error) {
        console.error(error);
    }
}