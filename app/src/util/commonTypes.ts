export const apiServerURL = process.env.NODE_ENV === "development" ? "http://localhost:3001/api/" : "/api/";
export const protectedURL = `${apiServerURL}protected/`

export interface CVData {
    AboutMe: AboutMe;
    EducationHistoryData: EducationHistory[];
    WorkExperienceDataRelevant: WorkExperience[];
    WorkExperienceDataOther: WorkExperience[];
    AchievementData: AchievementData;
    InterestData: Interest[];
    ProjectData: Project[];
    RefereeData: Referee[];
}

export interface AboutMe {
    profilePicture: string,
    lines: string[]
}

// LinkNewTab is the default case
type buttonType = 'defaultVisitWebsite' | 'defaultReferenceModal' | 'linkNewTab' | 'link';
export interface buttonConfig {
    type: buttonType,
    text?: string,
    link?: string
}

type educationProvider = 'University of Canterbury' | 'University of Waikato' | 'Cambridge High School';
export interface EducationHistory {
    provider: educationProvider,
    title: string,
    dates: string,
    markdown: string,
    logoUrl: string,
    link: string,
    buttons?: buttonConfig[]
};

type Employer = 'Vxt' | 'Gallagher' | 'Oppertunity Farming LTD' | 'ASB Bank' | 'McMillian Farms' | 'Onfarm Solutions' | 'Enatel';
export interface WorkExperience {
    employer: Employer,
    role: string,
    dates: string,
    markdown: string,
    logoUrl: string,
    link?: string,
    buttons?: buttonConfig[]
};

export interface Project {
    title: string,
    dates: string,
    blurb: string[]
    pictures?: { link: string, title: string }[]
    buttons?: buttonConfig[]
}
export interface AchievementData {
    blurb: string
};

export interface Interest {
    title: string,
    markdown: string
}


type Organization = educationProvider | Employer | 'College House';
export interface Referee {
    blurb: string[],
    organization: Organization,
    hidden?: boolean // default false
}