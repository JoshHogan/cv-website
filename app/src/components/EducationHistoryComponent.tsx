import React from 'react';
import { EducationHistory } from '../util/commonTypes';
import { COLOUR_GRAY, COLOUR_ORANGE } from '../constants';
import ButtonComponent from './ButtonComponent';
import ReactMarkdown from 'react-markdown';

export default function EducationHistoryComponent(props: EducationHistory) {
    return (
        <div style={{ margin: "20px" }}>
            <table style={{ textAlign: "left", margin: 0, verticalAlign: "top", marginBlock: 0, border: "0px solid black" }}>
                <tbody>
                    <tr>
                        <td valign="top" width={60} style={{ maxWidth: 60, padding: 5, paddingTop: 10 }}>
                            <a href={props.link}>
                                <img src={props.logoUrl} alt={props.provider + " logo"} width="100%" />
                            </a>
                        </td>
                        <td width="28%" rowSpan={2} valign="top">
                            <h2>
                                {props.provider}
                            </h2>
                            <p style={{ color: COLOUR_GRAY, textAlign: "left", margin: 0 }}>{props.dates}</p>
                        </td>
                        <td>
                            <h2 style={{ color: COLOUR_ORANGE }}>
                                {props.title}
                            </h2>
                            <ReactMarkdown source={props.markdown} />
                            {props.buttons?.map((value, index) => <ButtonComponent {...value} parent={props} key={index} />)}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}