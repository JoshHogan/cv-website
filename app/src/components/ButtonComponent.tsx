import React, { CSSProperties, useState } from 'react'
import { Overlay } from 'react-portal-overlay';
import RefereeComponent from './RefereeComponent';
import { buttonConfig, WorkExperience, EducationHistory } from '../util/commonTypes';
import { cvData } from '../util/cv-data';
import Button from 'react-bootstrap/Button';
import { COLOUR_ORANGE } from '../constants';

export default function ButtonComponent(props: buttonConfig & { parent?: WorkExperience | EducationHistory }) {
    const [open, setOpen] = useState(false);

    const buttonProperties: { style: CSSProperties, variant: string, size?: "sm" | "lg" } =
        { style: { backgroundColor: COLOUR_ORANGE, marginRight: "5px", marginTop: "5px" }, variant: "primary", size: "sm" }
    switch (props.type) {
        case "defaultVisitWebsite":
            return <Button {...buttonProperties} onClick={() => window.open(props.parent?.link, '_blank')}>Visit Website</Button>;
        case "defaultReferenceModal":
            const referee = cvData.RefereeData.find(x => x.organization === (props.parent as WorkExperience).employer || x.organization === (props.parent as EducationHistory).provider);
            if (!referee)
                return null;

            return <div style={{ display: "inline" }}>
                <Button {...buttonProperties} onClick={() => setOpen(true)}>Contact Referee</Button>
                <Overlay open={open} onClose={() => setOpen(false)} closeOnClick
                    style={{ background: "rgba(0, 0, 0, 0.3)", display: "flex", alignItems: "center", justifyContent: "center" }}>
                    <div style={{ background: "white", padding: "10px", borderRadius: "5px" }}
                    >
                        <RefereeComponent {...referee} />
                    </div>
                </Overlay>
            </div>;

        case "linkNewTab":
            return <Button {...buttonProperties} onClick={() => window.open(props.link, '_blank')}>{props.text}</Button>;
        case "link":
            return <Button {...buttonProperties} onClick={() => { if (props.link) window.location.href = props.link }}>{props.text}</Button>;

    }
}