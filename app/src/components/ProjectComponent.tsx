import React, { useState } from 'react';
import { COLOUR_ORANGE, COLOUR_GRAY, linkStyle } from '../constants';
import { Overlay } from 'react-portal-overlay';
import ButtonComponent from './ButtonComponent';
import { Project } from '../util/commonTypes';

export default function ProjectComponent(props: Project) {
    const [open, setOpen] = useState(false);
    return (
        <div style={{ margin: 10 }}>
            <h2 style={{ color: COLOUR_ORANGE }}>
                {props.title}
            </h2>
            <p style={{ color: COLOUR_GRAY }}>{props.dates}</p>
            {/* {props.blurb.map((value) => <p>{value}</p>)} */}
            <p>{props.blurb[0] + " "}
                {(props.pictures || props.blurb.length > 1) && <span style={linkStyle} onClick={() => setOpen(true)}>Show more</span>}
            </p>

            <Overlay open={open} onClose={() => setOpen(false)} closeOnClick
                animation={{ duration: 300, easing: 'ease' }}
                style={{ background: "rgba(0, 0, 0, 0.3)", display: "inline", alignItems: "center", justifyContent: "center"}}>
                <div style={{ background: "white", padding: "20px", borderRadius: "5px", maxWidth: 1000, marginTop: '50px', marginBottom: '50px', marginLeft: "auto", marginRight: "auto" }}>
                    <h2 style={{ color: COLOUR_ORANGE }}>
                        {props.title}
                    </h2>
                    <p style={{ color: COLOUR_GRAY }}>{props.dates}</p>
                    <br />
                    {props.blurb.map((value, index) => <p key={index}>{value}</p>)}
                    {props.buttons?.map((value, index) => <ButtonComponent {...value} key={index} />)}
                    {props.pictures &&
                        <div style={{ width: "90%" }}>
                            <br />
                            <h2 style={{ color: COLOUR_ORANGE, textAlign: "center" }}>{props.pictures[0].title}</h2>
                            {props.pictures?.map((x, i) => <img style={{ display: "block", margin: "auto", width: "70%" }} src={x.link} alt={x.title} key={i} />)}
                        </div>
                    }
                </div>
            </Overlay>
        </div>
    );
}