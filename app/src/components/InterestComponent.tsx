import React from 'react';
import { COLOUR_ORANGE } from '../constants';
import { Interest } from '../util/commonTypes';
import ReactMarkdown from 'react-markdown';

export default function InterestComponent(props: Interest) {
    return (
        <div style={{ margin: 10 }}>
            <h2 style={{ color: COLOUR_ORANGE }}>
                {props.title}
            </h2>
            {/* <p style={{ color: COLOUR_GRAY }}>{props.dates}</p> */}
            <ReactMarkdown source={props.markdown} />
        </div>
    );
}