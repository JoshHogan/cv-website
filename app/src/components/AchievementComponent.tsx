import React from 'react';
import { AchievementData } from '../util/commonTypes';
import ReactMarkdown from 'react-markdown';

export default function AchievementComponent(props: AchievementData) {
    return (
        <ReactMarkdown source={props.blurb} />
    );
}