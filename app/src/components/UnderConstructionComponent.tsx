import React from 'react'

export default function UnderConstructionComponent() {
    return (
        <img src={process.env.PUBLIC_URL + "/underConstruction.png"} width={"50%"} style={{ margin: "auto", display: "block" }} alt="Under Construction" />
    )
}
