import React from 'react';
import { COLOUR_ORANGE } from '../constants';
import { Referee } from '../util/commonTypes';

export default function RefereeComponent(props: Referee) {
    return (
        <div>
            {props.blurb.map((value, index) => {
                if (index === 0)
                    return <h2 style={{ color: COLOUR_ORANGE }} key={index}>{value}</h2>;
                else if (value.includes("@"))
                    return <a href={"mailto:" + value} onClick={() => window.open("mailto:" + value, '_blank')} key={index}>{value}</a>;
                else
                    return <p key={index}>{value}</p>;
            })
            }
        </div>
    );
}