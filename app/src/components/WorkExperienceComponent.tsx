import React from 'react';
import { COLOUR_GRAY, COLOUR_ORANGE } from '../constants';
import ButtonComponent from './ButtonComponent';
import { WorkExperience } from '../util/commonTypes';
import ReactMarkdowm from 'react-markdown';

export default function WorkExperienceComponent(props: WorkExperience) {
    return (
        <div style={{ margin: 10 }}>
            <table style={{ width: "100%", verticalAlign: "top", border: "0px solid black" }}>
                <tbody>
                    <tr>
                        <td valign="top" width={60} style={{ maxWidth: 60, padding: 5, paddingTop: 10 }}>
                            <a href={props.link}>
                                {props.logoUrl ? <img src={props.logoUrl} alt={props.employer + " logo"} width="100%" /> : undefined}
                            </a>
                        </td>
                        <td width="28%" rowSpan={2} valign="top">
                            <h2>
                                {props.employer}
                            </h2>
                            <p style={{ color: COLOUR_GRAY, margin: 0 }}>{props.dates}</p>
                        </td>
                        <td>
                            <h2 style={{ color: COLOUR_ORANGE }}>
                                {props.role}
                            </h2>
                            <ReactMarkdowm source={props.markdown} />
                            {props.buttons?.map((value, index) => <ButtonComponent {...value} parent={props} key={index} />)}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}