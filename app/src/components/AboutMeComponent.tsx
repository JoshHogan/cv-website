import React from 'react'
import { AboutMe } from '../util/commonTypes'
import ReactMarkdown from 'react-markdown';

export default function AboutMeComponent(props: AboutMe) {
    const paragraphStyle = { marginBottom: "10px" };
    return (
        <div>
            <div style={{ display: "inline-block" }}>
                <div style={{ minWidth: "150px", minHeight: "200px",float: "left", display: "flex", marginLeft: "20px", marginRight: "20px"}}>
                    <img width="175px" src={`${props.profilePicture}`} style={{ borderRadius: "50%"}} alt={"Profile"} />

                </div>
                {props.lines.map((value, index) => <div key={index} style={paragraphStyle}><ReactMarkdown source={value} /></div>)}
            </div>
        </div>
    )
}
