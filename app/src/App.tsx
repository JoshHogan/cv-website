import React, { CSSProperties, useEffect, useState } from 'react';
import './App.css';
import CVPage from './pages/CVPage';
import ContactPage from './pages/ContactPage';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Redirect
} from "react-router-dom";
import HomePage from './pages/HomePage';
import NotFound404 from './pages/NotFound404';
import { COLOUR_ORANGE } from './constants';
import { getCVData } from './util/cv-data';
import { useCookies } from 'react-cookie';
import queryString from 'query-string';
import { setupPing } from './util/ping';


type State = "fetching" | "success" | "no auth";

const routes: {
  name: string;
  path: string;
  homePage?: boolean;
  hidden?: boolean;
  component: () => JSX.Element;
}[]
  = [
    {
      name: "Home",
      path: "/",
      homePage: true,
      component: HomePage
    },
    {
      name: "CV",
      path: "/cv",
      component: CVPage
    },
    {
      name: "Contact Me",
      path: "/contact",
      component: ContactPage
    },
    {
      name: "404 Not Found",
      path: "/404",
      hidden: true,
      component: NotFound404
    }
  ]
function Root() {
  const [state, setState] = useState<State>("fetching");
  const [,setCookie,] = useCookies(['cookie-name']);

  useEffect(() => {
    (async function () {
      console.log("Root - Use Effect");

      const { token } = queryString.parse(window.location.search);

      if (token) {
        console.log("Setting Auth Token Cookie");
        setCookie("authToken", token);
        // window.location.href = window.location.href.split('?')[0];
        window.history.pushState({}, "test", window.location.href.split('?')[0]);
      }

      try {
        await getCVData();
        setupPing();
        setState("success");
        console.log(`Root - State "success"`);
      }
      catch (error) {
        console.warn(error);
        setState("no auth");
        console.log(`Root - State "no auth"`);
      }
    })()
  }, [setCookie]);


  return (
    <Router>
      <div className="App" style={{ maxWidth: 1200, margin: "auto" }}>
        <div className="topNav">
          {topNav()}
        </div>
        {body()}
      </div>
    </Router>
  );

  function topNav(): React.ReactNode {
    switch (state) {
      case "fetching":
        return routes.filter(x => x.hidden !== true).map((value, index) => <p key={index}>{value.name}</p>);
      case "no auth":
        return routes.filter(x => x.hidden !== true).map((value, index) => <p key={index} className={value.homePage ? "active" : ""}>{value.name}</p>);
      case "success":
        return routes.filter(x => x.hidden !== true).map((value, index) => <NavLink key={index} to={value.path} exact={value.homePage}>{value.name}</NavLink>)
    }
  }

  function body(): React.ReactNode {
    switch (state) {
      case "fetching":
        return <div style={pageStyle}><h1 style={headingStyle}>Loading...</h1></div>;
      case "no auth":
        if (window.location.pathname !== "/") window.location.pathname = "/";
        return <HomePage />;
      case "success":
        return (
          <Switch>
            {routes.map((value, index) => <Route key={index} path={value.path} component={value.component} exact={value.homePage} />)}
            <Redirect to="/404" />
          </Switch>);
    }
  }
}
export const pageStyle: CSSProperties =
{
  margin: 10
}
export const headingStyle: CSSProperties =
{
  textAlign: "center",
  background: COLOUR_ORANGE,
  color: "white"
}
export const spacerStyle: CSSProperties =
{
  padding: 30
}

export default Root;