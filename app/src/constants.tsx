export const COLOUR_ORANGE = "#FF8932";
export const COLOUR_GRAY = "gray";

export const linkStyle: React.CSSProperties = { cursor: "pointer", textDecorationLine: "underline", color: "rgb(0, 0, 238)"};