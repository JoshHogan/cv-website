import React from 'react'
import { headingStyle, pageStyle, spacerStyle } from '../App'
import AboutMeComponent from '../components/AboutMeComponent'
import { cvData } from '../util/cv-data'
import UnderConstructionComponent from '../components/UnderConstructionComponent'

export default function ContactPage() {
    return (
        <div style={pageStyle}>
            <h1 style={headingStyle}>About Me</h1>
            {cvData?.AboutMe ? <AboutMeComponent {...cvData.AboutMe} /> : undefined}
            <div style={spacerStyle} />

            <h1 style={headingStyle}>Contact Me</h1>
            <UnderConstructionComponent />
            <div style={spacerStyle} />
        </div>
    )
}