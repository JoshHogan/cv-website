import React from 'react'
import { headingStyle, pageStyle, spacerStyle } from '../App'
import ReactMarkdown from 'react-markdown';
import { cvData } from '../util/cv-data';
import ButtonComponent from '../components/ButtonComponent';
import logo from "../logo.svg";

export default function HomePage() {
    return (
        <div style={pageStyle}>
            <h1 style={headingStyle}>Josh's CV Website</h1>
            <div style={{ margin: "15px", textAlign: "center" }}>
                <ReactMarkdown source={`
After being tired of having to update a traditional CV, I decided to try something **new**.
                
At the same time I needed to learn React for my current part-time job at [Vxt](https://www.vxt.co.nz/).

Why not nail two birds with one stone?? And so, this project was born.
                
                `} />

                {cvData ?
                    <div>
                        <p>It appears that you are authenticated using a unique token so feel free to browse my <strong>CV</strong> </p> <ButtonComponent type="link" link="/cv" text="Click Here" />
                    </div>
                    :
                    <p>It appears that you don't have access to my <strong>CV</strong>.<br />
                    If you would like access or are having issues please don't hesitate to email me.</p>
                }

                <br />
                <ReactMarkdown source={`
If you find any problems/bugs report them to [joshua.t.hogan@gmail.com](mailto:joshua.t.hogan@gmail.com)
                `} />
            </div>
            <div style={spacerStyle} />
            <h1 style={headingStyle}>About the Website</h1>
            {/* { lines.map((value, index) =>{ return <p key={index} style={{}}>
                {value}
            </p>; }) } */}

            <div style={{ display: "inline" }}>
                <img src={logo} className="App-logo" alt="logo" />
                <p>This website is build entirely using React, written in TypeScript.</p>
                <p>There is a NodeJS API that serves the files and manages authentication.</p>
                <p>For more details see the <a href={"https://gitlab.com/JoshHogan/cv-website"}>GitLab Repo</a></p>
                <p>Gitlab CI/CD is used to deploy the backend and web app (using Docker containers) onto a Digital Ocean 'droplet'.</p>
            </div>
            <div style={spacerStyle} />
        </div >
    )
}