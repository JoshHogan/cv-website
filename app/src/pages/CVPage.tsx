import React from 'react';
import EducationHistoryComponent from '../components/EducationHistoryComponent';
import { cvData } from '../util/cv-data';
import WorkExperienceComponent from '../components/WorkExperienceComponent';
import AchievementComponent from '../components/AchievementComponent';
import RefereeComponent from '../components/RefereeComponent';
import ProjectComponent from '../components/ProjectComponent';
import InterestComponent from '../components/InterestComponent';
import { headingStyle, spacerStyle, pageStyle } from '../App';
import AboutMeComponent from '../components/AboutMeComponent';

export default function CVPage() {
    return (
        cvData ?
            <div style={pageStyle}>
                <h1 style={headingStyle}>About Me</h1>
                <AboutMeComponent {...cvData.AboutMe} />
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Education History</h1>
                {cvData.EducationHistoryData.map((value, index) => { return <div key={index}><EducationHistoryComponent {...value} /></div> })}
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Relevant Work Experience</h1>
                {cvData.WorkExperienceDataRelevant.map((value, index) => { return <div key={index}><WorkExperienceComponent {...value} /></div> })}
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Other Work Experience</h1>
                {cvData.WorkExperienceDataOther.map((value, index) => { return <div key={index}><WorkExperienceComponent {...value} /></div> })}
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Completed Projects</h1>
                {cvData.ProjectData.map((value, index) => { return <div key={index}><ProjectComponent {...value} /></div> })}
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Achievements</h1>
                <AchievementComponent {...cvData.AchievementData} />
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Interests</h1>
                {cvData.InterestData.map((value, index) => { return <div key={index}><InterestComponent {...value} /></div> })}
                <div style={spacerStyle} />

                <h1 style={headingStyle}>Referees</h1>
                <table width="100%" style={{ tableLayout: "fixed" }}>
                    <tbody>
                        <tr>
                            {cvData.RefereeData.filter(x => x.hidden !== true).map((value, index) => {
                                return <td key={index} style={{ verticalAlign: "top" }}><RefereeComponent {...value} /></td>
                            })}
                        </tr>
                    </tbody>
                </table>
                <div style={spacerStyle} />
            </div>
            :
            <div style={pageStyle}>
                <h1 style={headingStyle}>CV Loading...</h1>
            </div>
    )
}