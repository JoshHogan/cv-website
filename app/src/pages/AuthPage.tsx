import { RouteComponentProps, Redirect } from "react-router-dom";
import { useCookies } from "react-cookie";
import queryString from 'query-string';
import React from "react";

export default function AuthPage(props: RouteComponentProps) {
    const [cookies, setCookie, ] = useCookies(['cookie-name']);
    const { token, redirect } = queryString.parse(props.location.search);
  
    if (token)
      setCookie("authToken", token);
  
    if (token || cookies.authToken)
      return (<Redirect to={redirect as string || "/cv"} />);
    else
      return (<div></div>);
  }