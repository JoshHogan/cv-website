import React from 'react';
import { headingStyle, pageStyle } from '../App';

export default function NotFound404() {
    return (
        <div style={pageStyle}>
            <h1 style={headingStyle}>Not Found 404</h1>
            <h3>Hmm... the page you are looking for could not be found.</h3>
            <p>Please us the navbar links above to go back.</p>
        </div>
    )
}
