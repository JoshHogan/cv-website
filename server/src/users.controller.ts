import { Request, Response, NextFunction } from "express";
import { getAuthToken } from "./auth.controller";
import { getUserFromAuthToken, logEvent, clearEventLogs, getDatabase, addUserDatabase, deleteUserDatabase } from "./db";

export function logRequestMiddleware(req: Request, res: Response, next: NextFunction) {
    const authToken = getAuthToken(req);
    const user = getUserFromAuthToken(authToken);
    if (user && user.admin !== true) {
        if (req.url === "/ping")
            logEvent(user, 'ping');
        else
            logEvent(user, "httpRequest", req.url);
    }

    // Go to next no matter what
    next();
}

export function clearLogs(req: Request, res: Response) {
    try {
        clearEventLogs();
        res.redirect('./get');
    }
    catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
}

export function getUserData(req: Request, res: Response) {
    try {
        res.json(getDatabase());
    }
    catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
}

export function deleteUser(req: Request, res: Response) {
    const { name, token } = req.query;
    if (!name && !token) {
        res.send(400);
    } else {
        if (deleteUserDatabase(name as string, token as string))
            res.redirect("./get");
        else
            res.sendStatus(404);
    }
}
export function addUser(req: Request, res: Response) {
    const { name, token } = req.body;
    if (!name || !token) {
        res.send(400);
    }
    else {
        addUserDatabase(name, token);
        res.redirect('./get');
    }
}

export function addUserForm(req: Request, res: Response) {
    return res.send(`
    <html>
    <form method="post">`
        +
        ["name", "token"].map(x => `<input name="${x}" placeholder="${x}"/>`).join("<br /><br/>")
        +
        `
    <br/>
    <br/>
    <button type="submit">Submit</button>
    </form>
    </html>`);
}