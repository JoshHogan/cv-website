import express from 'express';
import http from 'http';
import https from 'https';
import path from 'path';
import fs from 'fs';
import bodyParser from 'body-parser';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { APIRoutes } from './api.routes';
import { ReactRoutes } from './react.routes';

const key = fs.readFileSync(path.join(__dirname, "..", "certs", "privkey.pem"));
const cert = fs.readFileSync(path.join(__dirname, "..", "certs", "fullchain.pem"));

const app = express();
const serverHTTP = http.createServer(app);
const serverHTTPS = https.createServer({key: key, cert: cert}, app);
const port = 3001;

app.set('json spaces', 2);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(cors({
    origin: process.env.NODE_ENV === "development" ? ['http://localhost:3000'] : undefined,
    credentials: true
}));

APIRoutes.initRoutes(app);
ReactRoutes.initRoutes(app);


serverHTTP.listen(port, () => console.log("Server running on http://localhost:" + port));
serverHTTPS.listen(port + 1, () => console.log("Server running on https://localhost:" + (port + 1)));




