import express, { Express } from 'express';
import path from 'path';
import { cvData } from '../protected/cv-data';
import { authenticateRequestAsAdmin, authenticateRequestAsUser } from './auth.controller';
import { logRequestMiddleware, clearLogs, getUserData, addUserForm, addUser, deleteUser } from './users.controller';

export class APIRoutes {
    static initRoutes(app: Express) {
        app.get("/api", (req, res) => res.send("Hello World"));
        app.get("/api/version", (req, res) => res.send(process.env.npm_package_version || "unknown"));

        app.use("/api", logRequestMiddleware);
        app.get("/api/ping", (req, res) => res.sendStatus(200));
        
        app.use("/api/user", authenticateRequestAsAdmin);
        app.get("/api/user/clear", clearLogs);
        app.get("/api/user/get", getUserData);
        app.get("/api/user/add", addUserForm);
        app.post("/api/user/add", addUser);
        app.get("/api/user/delete", deleteUser);

        app.use("/api/public", express.static(path.join(__dirname, '../public')));

        app.use("/api/protected", authenticateRequestAsUser);
        app.get("/api/protected/cvData.json", (req, res) => res.json(cvData));
        app.use("/api/protected", express.static(path.join(__dirname, '../protected')));

    }
}


