import express, { Express } from 'express';
import path from 'path';


export class ReactRoutes {
    static initRoutes(app: Express) {
        app.use(express.static(path.join(__dirname, "..", "..", "app", "build")));
        app.get("*", (req, res) => res.sendFile(path.join(__dirname, "..", "..", "app", "build", "index.html")));
    }
}