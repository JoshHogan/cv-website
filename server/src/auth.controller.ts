import { Request, Response, NextFunction } from 'express'
import { getUserFromAuthToken } from './db';

export function authenticateRequestAsAdmin(req: Request, res: Response, next: NextFunction) {
    return authenticateRequest(req, res, next, true);
}
export function authenticateRequestAsUser(req: Request, res: Response, next: NextFunction) {
    return authenticateRequest(req, res, next, false);
}

async function authenticateRequest(req: Request, res: Response, next: NextFunction, isAdmin: boolean) {
    try {
        const authToken = getAuthToken(req);
        if (authToken == null) {
            res.sendStatus(401);
            console.warn("No auth given for: " + req.url);
            return;
        }

        const user = getUserFromAuthToken(authToken);
        if (user && user.enabled && (!isAdmin || user.admin)) {
            console.log(`Authed User${isAdmin ? " as Admin" : ""}: ${user.name} for ${req.url}`);
            next();
        }
        else {
            console.warn("Forbidden auth given for: " + req.url);
            res.sendStatus(403);
        }
    }
    catch (error) {
        console.warn("Internal error in authenticate request: " + error);
        res.sendStatus(500);
    }
}

export function getAuthToken(req: Request) {
    return req.cookies.authToken as string | undefined;
}
