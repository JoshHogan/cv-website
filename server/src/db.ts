import Low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

type event = 'ping' | 'httpRequest';
interface Log {
    date: number;
    dateString: string;
    additionalInfo?: string;
}
export interface User {
    name: string;
    enabled: boolean;
    token: string;
    logs: { [key in event]: Log[]; };
    admin?: boolean;
}

interface DBSchema {
    users: User[];
}


export function getUserFromAuthToken(authToken: string | undefined): User | undefined {
    if (!authToken)
        return undefined;

    return db.get('users').find(x => x.token === authToken).value();
}

export function logEvent(user: User, event: event, additionalInfo?: string) {
    const userDB = db.get('users').find(x => x.token === user.token);
    const now = new Date();
    const logEntry: Log = { dateString: now.toString(), date: now.getTime(), additionalInfo: additionalInfo };

    console.log("Logging Event: " + JSON.stringify(logEntry));
    userDB.get("logs").get(event).push(logEntry).write();
}

export function clearEventLogs() {
    db.get('users').forEach(user => user.logs = { httpRequest: [], ping: [] }).write();
}

export function getDatabase() {
    return db.value();
}

export function addUserDatabase(name: string, token: string) {
    db.get('users').push({ name: name, token: token, enabled: true, logs: { httpRequest: [], ping: [] } }).write();
}

export function deleteUserDatabase(name?: string, token?: string): boolean {
    if (name || token) {
        const user = db.get("users").find(x => (x.name === name || !name) && (x.token === token || !token)).value();
        if (user) {
            db.get("users").remove(user).write();
            return true;
        }
    }
    return false;
}
const db = Low(new FileSync<DBSchema>('database.json'));
db.defaults({ 'users': [] }).write();