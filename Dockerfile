FROM node:12.16.1-alpine3.10 as baseOS
RUN echo "Using: node:12.16.1-alpine3.10"

FROM baseOS as builderApp
RUN mkdir -p /src/app
WORKDIR /src
COPY app/package.json app/package-lock.json /src/app/
RUN cd app && npm install
COPY app /src/app
RUN cd app && npm run build


FROM baseOS as builderServer
RUN mkdir -p /src/server
WORKDIR /src
COPY server/package.json server/package-lock.json /src/server/
RUN cd server && npm install
COPY server /src/server
COPY app/src/util/commonTypes.ts /src/app/src/util/commonTypes.ts
RUN cd server && npm run build
RUN cp -rl /src/server/public /src/server/build/server/ && \
    cp -rl /src/server/protected /src/server/build/server/

FROM builderServer as prod
COPY --from=builderApp /src/app/build /src/server/build/app/build
WORKDIR /src/server
EXPOSE 3001 3002
CMD ["npm", "run", "prod"]